# HIGHWAY CROSSING FROG

## How to build
1. checkout this repository
2. call `git submodule update --init --recursive` to download cocos2d-x
3. `cd` to the `cocos2d` directory and run `python download-deps.py`
4. leave the project directory alltogether and create a separate directory outside. I suggest calling it "hwcfrog-build"
5. inside `hwcfrog-buid`, run `cmake ../hwcfrog`. replace `../hwcfrog` with the path to this project.
6. run `make`

### If you have a Macintosh
Cocos2d-x 3.17 has a bug with OSX and GameController.framework. You will need to apply this pull request to the `cocos2d` submodule to fix it.

[Fix OSX linker error with missing GameController.framework #18926](https://github.com/cocos2d/cocos2d-x/pull/18926)
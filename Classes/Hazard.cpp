#include "Hazard.h"

using namespace cocos2d;

bool Hazard::init(){
    return Node::init();
}

Hazard* Hazard::createWithLength(int length){
    Hazard *ret = Hazard::create();

    if(length > 2){
        ret->addChild(Sprite::createWithSpriteFrameName("truck-start.bmp"));
        for(int i = 1; i < length; i++){
            Sprite* sprite = Sprite::createWithSpriteFrameName("truck-cont.bmp");
            sprite->setPosition(i*32, 0);
            ret->addChild(sprite);
        }
        Sprite* truckbutt = Sprite::createWithSpriteFrameName("truck-end.bmp");
        truckbutt->setPosition(length*32, 0);
        ret->addChild(truckbutt);
        PhysicsBody* dangerZone = PhysicsBody::createBox(
                    Size((length * 32), 16),
                    PHYSICSBODY_MATERIAL_DEFAULT,
                    Vec2(length * 16, 0));
        ret->addComponent(dangerZone);
    } else if (length == 2) {
        ret->addChild(Sprite::createWithSpriteFrameName("car-start.bmp"));
        Sprite* carbutt = Sprite::createWithSpriteFrameName("car-end.bmp");
        carbutt->setPosition(32, 0);
        ret->addChild(carbutt);
        PhysicsBody* dangerZone = PhysicsBody::createBox(
                    Size(32, 16),
                    PHYSICSBODY_MATERIAL_DEFAULT,
                    Vec2(16, 0));
        ret->addComponent(dangerZone);
    } else {
        // this shouldn't happen, but like, idk, put a turtle.
        // that way we know there's a hazard where there shouldn't be.
        ret->addChild(Sprite::createWithSpriteFrameName("turtle-up.bmp"));
        ret->setTag(12);
    }

    ret->getPhysicsBody()->setCategoryBitmask(FDEF_HAZARD);
    ret->getPhysicsBody()->setContactTestBitmask(FDEF_FROG | FDEF_CLEANER | FDEF_SPAWN);
    ret->getPhysicsBody()->setGroup(FDEF_HAZARD); // hazards
    ret->setTag(FDEF_HAZARD);

    return ret;
}

void Hazard::update(float delta){
    this->setPositionX(this->getPositionX() - (delta * 32 * 2));
}

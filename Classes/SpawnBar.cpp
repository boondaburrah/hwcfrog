#include "SpawnBar.h"

using namespace cocos2d;

bool SpawnBar::init(){
    if(!Node::init()){
        return false;
    }

    this->setTag(FDEF_SPAWN);
    this->spawnType = FDEF_SPAWN_HIGHWAY;

    PhysicsBody* bumper = PhysicsBody::createBox(Size(FDEF_TILESIZE, FDEF_TILESIZE*5));
    bumper->setDynamic(false);
    bumper->setCategoryBitmask(FDEF_SPAWN);
    bumper->setContactTestBitmask(FDEF_EVERYTHING ^ FDEF_FROG);
    bumper->setGroup(FDEF_SPAWN);
    bumper->setCollisionBitmask(0);

    this->addComponent(bumper);

    this->spawn = EventListenerPhysicsContactWithGroup::create(FDEF_SPAWN);
    this->spawn->onContactSeparate = [=](PhysicsContact &c){
        cocos2d::log("SPAWN");
        auto a = c.getShapeA()->getBody()->getNode();
        auto b = c.getShapeB()->getBody()->getNode();
        float y = 0;
        if(a->getTag() != FDEF_SPAWN){
            y = a->getPositionY();
        }
        if(b->getTag() != FDEF_SPAWN){
            y = b->getPositionY();
        }

        // crazy ass workaround because addChild doesn't work properly inside a physics event handler if the child has a physics component.
        this->runAction(CallFunc::create([this, y](){SpawnBar::createLandHazard(this, y);}));
    };

    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(this->spawn, this);

    return true;
}

void SpawnBar::createLandHazard(SpawnBar *self, int y){
    cocos2d::log("SPAWN ACTION");
    Hazard* haz = Hazard::createWithLength((rand() % 3) + 2);
    haz->setPosition(((rand() % 10) + 3) * FDEF_TILESIZE, y);
    self->addChild(haz);
}

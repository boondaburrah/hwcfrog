#include "Frog.h"

using namespace cocos2d;

bool Frog::init(){
    if(!Node::init()){
        return false;
    }

    Sprite* frog = Sprite::createWithSpriteFrameName("frog.bmp");
    this->addChild(frog);
    PhysicsBody *frogHitbox = PhysicsBody::createBox(Size(16,16));
    frogHitbox->setCategoryBitmask(FDEF_FROG); // I am number 1
    frogHitbox->setContactTestBitmask(FDEF_EVERYTHING); // anything
    frogHitbox->setDynamic(false); // don't get knocked around.
    this->setPosition(128+16, 32+16);
    this->addComponent(frogHitbox);
    this->setTag(FDEF_FROG);

    this->movement = EventListenerKeyboard::create();
    movement->onKeyPressed = [this](EventKeyboard::KeyCode k, Event* e){
        switch (k) {
        case EventKeyboard::KeyCode::KEY_UP_ARROW:
        case EventKeyboard::KeyCode::KEY_W:
            cocos2d::log("FROG UP");
            this->setRotation(0);
            this->runAction(MoveBy::create(0.1, Vec2(0, FDEF_TILESIZE)));
            break;
        case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
        case EventKeyboard::KeyCode::KEY_S:
            cocos2d::log("FROG DOWN");
            this->setRotation(180);
            this->runAction(MoveBy::create(0.1, Vec2(0, -FDEF_TILESIZE)));
            break;
        case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
        case EventKeyboard::KeyCode::KEY_A:
            cocos2d::log("FROG LEFT");
            this->setRotation(270);
            this->runAction(MoveBy::create(0.1, Vec2(-FDEF_TILESIZE, 0)));
            break;
        case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
        case EventKeyboard::KeyCode::KEY_D:
            cocos2d::log("FROG RIGHT");
            this->setRotation(90);
            this->runAction(MoveBy::create(0.1, Vec2(FDEF_TILESIZE, 0)));
            break;
        default:
            break;
        }
    };

    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(this->movement, this);

    return true;
}

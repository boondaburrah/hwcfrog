#ifndef __HIGHWAYSCENE_HEADER__
#define __HIGHWAYSCENE_HEADER__

#include "cocos2d.h"
#include "frogdefs.h"
#include "Hazard.h"
#include "Cleaner.h"
#include "SpawnBar.h"
#include "Lifesaver.h"
#include "Frog.h"

class Highway : public cocos2d::Scene {
private:
    cocos2d::SpriteFrameCache *spriteCache;
public:
    virtual bool init();
    static cocos2d::Scene* createScene();
    CREATE_FUNC(Highway)
};

#endif

#ifndef __FROG_H__
#define __FROG_H__

#include "cocos2d.h"
#include "frogdefs.h"

class Frog : public cocos2d::Node {
public:
    cocos2d::EventListenerKeyboard* movement;
    bool init() override;
    CREATE_FUNC(Frog)
};

#endif

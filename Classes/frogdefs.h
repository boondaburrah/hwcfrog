#ifndef __FROGDEFS_H__
#define __FROGDEFS_H__

// convenience
#define FDEF_TILESIZE 32

// collision settings
#define FDEF_FROG    0b00000001
#define FDEF_HAZARD  0b00000010
#define FDEF_WATER   0b00000100
#define FDEF_SAFE    0b00001000
#define FDEF_CLEANER 0b00010000
#define FDEF_SPAWN   0b00100000
#define FDEF_EVERYTHING 0b11111111

// I should probably have done some of these with an enum huh.
#define FDEF_SPAWN_HIGHWAY  false
#define FDEF_SPAWN_WATER    true

#endif

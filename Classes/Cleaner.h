#ifndef __CLEANER_H__
#define __CLEANER_H__

#include "cocos2d.h"
#include "frogdefs.h"

class Cleaner : public cocos2d::Node {
public:
    cocos2d::EventListenerPhysicsContactWithGroup *cleanup;
    bool init() override;
    CREATE_FUNC(Cleaner)
};

#endif

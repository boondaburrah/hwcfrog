#include <HighwayScene.h>

using namespace cocos2d;

bool Highway::init(){
    if(!Scene::initWithPhysics()){
        return false;
    }

    this->spriteCache = SpriteFrameCache::getInstance();

    this->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_SHAPE);
    this->getPhysicsWorld()->setGravity(Vec2::ZERO);

    Sprite* titleScreen = Sprite::create("title.png");
    titleScreen->setAnchorPoint(Vec2::ZERO);
    Label* titleLabel = Label::createWithTTF("Press any key to start", "fonts/Marker Felt.ttf", 20);
    titleLabel->setPosition(160, 160); // trust me, I'm a doctor.
    titleScreen->addChild(titleLabel); // a sprite is a node so we can cheat for now. This should get cleaned up when the title gets deleted.
    this->addChild(titleScreen, 100, "title");

    TMXTiledMap *map = TMXTiledMap::create("highway.tmx");
    this->addChild(map);

    Frog* frog = Frog::create();
    this->addChild(frog);

    EventListenerKeyboard *keys = EventListenerKeyboard::create();
    keys->onKeyPressed = [this](EventKeyboard::KeyCode k, Event* e){
        if(this->getChildByName("title")){
            this->getChildByName("title")->removeFromParentAndCleanup(true);
        }
        switch (k) {
        case EventKeyboard::KeyCode::KEY_ESCAPE:
        case EventKeyboard::KeyCode::KEY_Q:
            Director::getInstance()->end();
            break;
        default:
            return false;
            break;
        }
        return true;
    };

    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keys, this);

    EventListenerPhysicsContactWithGroup *contact = EventListenerPhysicsContactWithGroup::create(FDEF_HAZARD); // hazards
    contact->onContactBegin = [](PhysicsContact &c){
        cocos2d::log("TRUCK CONTACT!");
        auto left = c.getShapeA()->getBody()->getNode();
        auto right = c.getShapeB()->getBody()->getNode();
        if(left && right){
            if((left->getTag() == FDEF_FROG) || (right->getTag() == FDEF_FROG)){
                cocos2d::log("FROG SQUISH");
            }
        }

        return true;
    };

    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contact, this);

    // NOT YET USED
    EventListenerPhysicsContactWithGroup *sinking = EventListenerPhysicsContactWithGroup::create(FDEF_WATER); // water
    sinking->onContactBegin = [](PhysicsContact &c){
        cocos2d::log("MAN OVERBOARD");
        auto left = c.getShapeA()->getBody()->getNode();
        auto right = c.getShapeB()->getBody()->getNode();
        if(left && right){
            if((left->getTag() == FDEF_FROG) || (right->getTag() == FDEF_FROG)){
                cocos2d::log("FROG SINK");
            }
        }

        return true;
    };

    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(sinking, this);

    Lifesaver* log = Lifesaver::createLog(3, 2);
    log->setPosition(32+16, 384+16);
    this->addChild(log);

    // SET UP THE CARS
    Cleaner* leftClean = Cleaner::create();
    leftClean->setPosition(-16, FDEF_TILESIZE*5 + 16);
    this->addChild(leftClean);

    SpawnBar* rightSpawn = SpawnBar::create();
    rightSpawn->setPosition(FDEF_TILESIZE*10 + 16, FDEF_TILESIZE*5 + 16);
    this->addChild(rightSpawn);

    SpawnBar::createLandHazard(rightSpawn, FDEF_TILESIZE * 2);
    SpawnBar::createLandHazard(rightSpawn, FDEF_TILESIZE);
    SpawnBar::createLandHazard(rightSpawn, 0);
    SpawnBar::createLandHazard(rightSpawn, -FDEF_TILESIZE);
    SpawnBar::createLandHazard(rightSpawn, -FDEF_TILESIZE * 2);
    // CARS SHOULD BE ALL SET NOW

    return true;
}

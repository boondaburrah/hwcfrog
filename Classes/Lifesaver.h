#ifndef __LIFESAVER_HEADER__
#define __LIFESAVER_HEADER__

#include "cocos2d.h"
#include "frogdefs.h"

class Lifesaver : public cocos2d::Node {
private:
    float vector;
public:
    bool init() override;
    void update(float delta);
    static Lifesaver* createLog(int length, float vector);
    static Lifesaver* createTurtles(int length, float vector);
    CREATE_FUNC(Lifesaver)
};

#endif

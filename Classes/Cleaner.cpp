#include "Cleaner.h"

using namespace cocos2d;

bool Cleaner::init(){
    if(!Node::init()){
        return false;
    }

    this->setTag(FDEF_CLEANER);

    PhysicsBody* bumper = PhysicsBody::createBox(Size(FDEF_TILESIZE, FDEF_TILESIZE*5));
    bumper->setDynamic(false);
    bumper->setCategoryBitmask(FDEF_CLEANER);
    bumper->setContactTestBitmask(FDEF_EVERYTHING ^ FDEF_FROG);
    bumper->setGroup(FDEF_CLEANER);
    bumper->setCollisionBitmask(0);

    this->addComponent(bumper);

    this->cleanup = EventListenerPhysicsContactWithGroup::create(FDEF_CLEANER);
    this->cleanup->onContactSeparate = [](PhysicsContact &c){
        cocos2d::log("CLEANUP");
        auto a = c.getShapeA()->getBody()->getNode();
        auto b = c.getShapeB()->getBody()->getNode();
        if(a->getTag() != FDEF_CLEANER){
            cocos2d::log("removing thing a");
            a->removeFromParentAndCleanup(true);
        }
        if(b->getTag() != FDEF_CLEANER){
            cocos2d::log("removing thing b");
            b->removeFromParentAndCleanup(true);
        }
    };

    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(this->cleanup, this);

    return true;
}

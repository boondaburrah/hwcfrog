#ifndef __SPAWNBAR_H__
#define __SPAWNBAR_H__

#include "cocos2d.h"
#include "frogdefs.h"
#include "Hazard.h"

class SpawnBar : public cocos2d::Node {
private:
    bool spawnType;
public:
    cocos2d::EventListenerPhysicsContactWithGroup *spawn;
    bool init() override;
    static void createLandHazard(SpawnBar *self, int y);
    CREATE_FUNC(SpawnBar)
};

#endif

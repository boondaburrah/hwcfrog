#include "Lifesaver.h"

using namespace cocos2d;

bool Lifesaver::init(){
    if(!Node::init()){
        return false;
    };

    this->scheduleUpdate(); // I Have no idea why Lifesavers need this and Hazards dont.
    // I'm suspicious it has to do with Hazards having a PhysicsBody.

    return true;
}

Lifesaver* Lifesaver::createLog(int length, float vector){
    Lifesaver* ret = Lifesaver::create();
    if(!ret){
        return ret;
    }

    ret->vector = vector;

    ret->addChild(Sprite::createWithSpriteFrameName("log-start.bmp"));
    for(int i = 1; i < length; i++){
        Sprite* longlog = Sprite::createWithSpriteFrameName("log-cont.bmp");
        longlog->setPositionX(i*32);
        ret->addChild(longlog);
    }
    Sprite* logbutt = Sprite::createWithSpriteFrameName("log-end.bmp");
    logbutt->setPositionX(length*32);
    ret->addChild(logbutt);

    return ret;
}

void Lifesaver::update(float delta){
    this->setPositionX(this->getPositionX() + (delta * FDEF_TILESIZE * this->vector));
}

// IDK I probably won't get to using this.
Lifesaver* Lifesaver::createTurtles(int length, float vector){
    Lifesaver* ret = Lifesaver::create();
    if(!ret){
        return ret;
    }

    return ret;
}

#ifndef __HAZARD_HEADER__
#define __HAZARD_HEADER__
#include "cocos2d.h"
#include "frogdefs.h"

class Hazard : public cocos2d::Node {
public:
    CREATE_FUNC(Hazard)
    bool init();
    void update(float delta);
    static Hazard* createWithLength(int length);
};

#endif
